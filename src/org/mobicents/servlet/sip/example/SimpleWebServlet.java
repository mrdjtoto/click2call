/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.mobicents.servlet.sip.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.sip.ConvergedHttpSession;
import javax.servlet.sip.SipApplicationSession;
import javax.servlet.sip.SipFactory;
import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipSession;
import javax.servlet.sip.URI;

import org.apache.log4j.Logger;

public class SimpleWebServlet extends HttpServlet
{ 	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SimpleWebServlet.class);
	private SipFactory sipFactory;	

	//Questo estende Servlet, non ServletSIP
	@Override
	public void init(ServletConfig config) throws ServletException {		
		super.init(config);
		logger.info("the SimpleWebServlet has been started");
		try { 			
			// Getting the Sip factory from the JNDI Context
			Properties jndiProps = new Properties();			
			Context initCtx = new InitialContext(jndiProps);
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			sipFactory = (SipFactory) envCtx.lookup("sip/org.mobicents.servlet.sip.example.SimpleApplication/SipFactory");
			logger.info("Sip Factory ref from JNDI : " + sipFactory);
		} catch (NamingException e) {
			throw new ServletException("Uh oh -- JNDI problem !", e);
		}
	}
    /**
     * Handle the HTTP GET method by building a simple web page.
     */
	
	//Dalla pagina JSP c'è un GET, allora gestiamo il doGet.
    public void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        String toAddr = request.getParameter("to"); 			 //to e from sono passati dalla pagina JSP.
        String fromAddr = request.getParameter("from");   //to e from sono passati dalla pagina JSP.
        String bye = request.getParameter("bye"); 			 // controllo se per caso contiene il bye.

        URI to = toAddr == null ? null : sipFactory.createAddress(toAddr).getURI();    //to e from sono due Array. Chiedo alla sipFactory di creare gli oggetti URI
        URI from = fromAddr == null ? null : sipFactory.createAddress(fromAddr).getURI();    

        //controllo se ci sono chiamate attive.
        CallStatusContainer calls = (CallStatusContainer) getServletContext().getAttribute("activeCalls");

        // Creo la SIP APPLICATION SESSION <<<<<<<------------------------------------------------
        // E' il contenitore dei contenitori che contiene le due sessioni sip con i due client e con la http.
        // Me la faccio restiruire dalla request, mi prendo dalla sessione, l'application session. NOTA: questo su
        // un tomcat normale restituisce errore perché non ha l'application session.
        // Si prende dalla sessione HTTP l'application session, che è più in alto.
        SipApplicationSession appSession =  ((ConvergedHttpSession)request.getSession()).getApplicationSession();

        if(bye != null) {
        	if(bye.equals("all")) {
        		Iterator it = (Iterator) appSession.getSessions("sip");
        		while(it.hasNext()) {
        			SipSession session = (SipSession) it.next();
        			Call call = (Call) session.getAttribute("call");
        			call.end();
        			calls.removeCall(call);
        		}
        	} else {
        		// Someone wants to end an established call, send byes and clean up
        		
        		
        		Call call = calls.getCall(fromAddr, toAddr);
        		call.end();
        		calls.removeCall(call);
        	}
        } else {
        	if(calls == null) {
        		calls = new CallStatusContainer();
        		getServletContext().setAttribute("activeCalls", calls);
        	}
        	
        	// Add the call in the active calls
        	Call call = calls.addCall(fromAddr, toAddr, "FFFF00");
        	
        	
        	//Qui creo la chiamata tramite BROWSER. Creo una richiesta sip, chiedendola alla sipFactory
        	//all'interno della appSession, di tipo INVITE da from a to.
        	SipServletRequest req = sipFactory.createRequest(appSession, "INVITE", from, to);

        	// Set some attribute
        	// con req.getSession mi torna la sessione SIP e ci metto un attributo "secondaryPartyAddress"
        	// che conterrà il from che sarà il to della seconda sessione che devo andare a creare
        	req.getSession().setAttribute("SecondPartyAddress", sipFactory.createAddress(fromAddr));
        	
        	//Nella sessione SIP mi conservo un oggetto chiamata contentente le informazioni utili della chiamata.
        	req.getSession().setAttribute("call", call);
        	
        	// This session will be used to send BYE
        	call.addSession(req.getSession());
        	
        	logger.info("Sending request" + req);
        	// Send the INVITE request            
        	req.send();
        }
        
        // Write the output html
    	PrintWriter	out;
        response.setContentType("text/html");
        out = response.getWriter();
        
        // Just redirect to the index
        out.println("<HTML><META HTTP-EQUIV=\"Refresh\"CONTENT=\"0; URL=index.jsp\"><HEAD><TITLE></HTML>");
        out.close();
    }
}

//Adesso quando il primo telefono risponderà verrà inviato un 200 OK che verrà gestito dall'altra applicazione.