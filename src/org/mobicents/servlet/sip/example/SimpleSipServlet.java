/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011, Red Hat, Inc. and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.mobicents.servlet.sip.example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.sip.Address;
import javax.servlet.sip.SipErrorEvent;
import javax.servlet.sip.SipErrorListener;
import javax.servlet.sip.SipFactory;
import javax.servlet.sip.SipServlet;
import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;
import javax.servlet.sip.SipSession;
import javax.servlet.sip.URI;

import org.kurento.client.HubPort;
import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;
import org.kurento.client.PlayerEndpoint;
import org.kurento.client.RtpEndpoint;
import org.kurento.client.Composite;
import support.GroupCall;
import support.KurentoSupport;
import org.apache.log4j.Logger;

public class SimpleSipServlet extends SipServlet implements SipErrorListener,
		Servlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SimpleSipServlet.class);
	private static final String CONTACT_HEADER = "Contact";
	private SipFactory sipFactory;
	private static final String DEBUG =            "\n\n> > > > >   D   E   B   U   G   > > > > > > \n\n";
	private static final String KURENTO_DEB = "\n\n☎ ☎ ☎ ☎ ☎  D   E   B   U   G   ☎ ☎ ☎ ☎ ☎\n\n";
	private static final String LINE_DEB =        "\n\n=============================\n";
	
	Map <String, URI> registeredUsers;// = new HashMap<String, URI>();
	Map<String, GroupCall> callContainer = new HashMap<String, GroupCall>();
	//String file_uri = "http://files.kurento.org/video/filter/puerta-del-sol.ts";
	String file_uri = "http://video.webmfiles.org/elephants-dream.webm";
	String file_uri2 = "http://video.blendertestbuilds.de/download.blender.org/peach/trailer_480p.mov";

	public SimpleSipServlet() { /*...*/ }
	
	//In init mi fa restituire la fabbrica SIP per una applicazione che si chiama click2call
	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		logger.warn("\n\n: : : : : : : : : : : : : : : : : : : : : : : : : :   SIP SERVLET STARTED !!!   : : : : : : : : : : : : : : : : : : : : : : : : : :\n");
		try { 			
			// Getting the Sip factory from the JNDI Context
			Properties jndiProps = new Properties();			
			Context initCtx = new InitialContext(jndiProps);
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			sipFactory = (SipFactory) envCtx.lookup("sip/org.mobicents.servlet.sip.example.SimpleApplication/SipFactory");
			logger.info("Sip Factory ref from JNDI : " + sipFactory);
		} catch (NamingException e) {
			throw new ServletException("Uh oh -- JNDI problem !", e);			
		}
	}
	
	@Override
	protected void doInvite(SipServletRequest req) throws ServletException, IOException {
		SipSession session = req.getSession();
		SipSession linkedSession = (SipSession) session.getAttribute("LinkedSession");
		
		logger.warn("\n\n★ ★ ★ ★ ★ ★ ★ ★ ★ ★   I N V I T E   ★ ★ ★ ★ ★ ★ ★ ★ ★ ★\n");
		logger.info(DEBUG + "From: " + req.getFrom() + "\nTo: " + req.getTo().toString() + LINE_DEB);
		String voip1_rtp1 = new String(req.getRawContent());
		//This will be our reference to the room.
		String roomName = KurentoSupport.stringfy(req.getTo().toString());

		 if (KurentoSupport.contains(req.getTo().toString(), "kurentohub")) {
			logger.warn("\n\n☎ ☎ ☎ ☎ ☎  User has called KURENTOhub ☎ ☎ ☎ ☎ ☎\n");
			KurentoClient kurento = KurentoSupport.connect();
			logger.info(KURENTO_DEB + "Connected to Kurento with [ COMPOSITE  -> HUBPORT ]." + LINE_DEB );
			MediaPipeline pipeline = kurento.createMediaPipeline();
			Composite composite = new Composite.Builder(pipeline).build();
		    HubPort hubPort1 = new HubPort.Builder(composite).build();
		    HubPort hubPort2 = new HubPort.Builder(composite).build();
		    HubPort hubPort3 = new HubPort.Builder(composite).build();

		    RtpEndpoint rtpEP = new RtpEndpoint.Builder(pipeline).build();
			String inSdpAnswer = rtpEP.processOffer(voip1_rtp1);
			logger.info(KURENTO_DEB + "RtpEndpoint created." + LINE_DEB );
			PlayerEndpoint player = new PlayerEndpoint.Builder(pipeline, file_uri).build();
			PlayerEndpoint player2 = new PlayerEndpoint.Builder(pipeline, file_uri2).build();
			logger.info(KURENTO_DEB + "PlayerEndpoints created." + LINE_DEB );
			player.connect(hubPort1);
			player2.connect(hubPort2);
			hubPort3.connect(rtpEP);
			logger.info(KURENTO_DEB + "Offer for RtpEndpoint processed by itself. " + LINE_DEB );
			SipServletResponse response = req.createResponse(200);
			response.setContent(inSdpAnswer, "application/sdp");
			logger.info(KURENTO_DEB + "200 OK generated" + LINE_DEB);
			player.play();
			player2.play();
			logger.info(KURENTO_DEB + "Players started." + LINE_DEB);
			response.send();
			logger.info(KURENTO_DEB + "200 OK sent." +  KURENTO_DEB);
		
		} else if (KurentoSupport.contains(req.getTo().toString(), "groupcall")) {
			String fromName = KurentoSupport.stringfy(req.getFrom().toString()); 
			roomName = GroupCall.stringfy(req.getTo().toString());
			
// GROUP CALL 
			logger.warn("\n\n☎ ☎ ☎ ☎ ☎  User has called GROUPCALL (" + roomName + ") ☎ ☎ ☎ ☎ ☎\n");			
			GroupCall call = null;
			if (callContainer.containsKey(roomName)) {
				call = callContainer.get(roomName);
			} else {  call = new GroupCall(); }
				GroupCall.connect();
			if (!callContainer.containsKey(roomName)) {
				logger.info(KURENTO_DEB + "Room '" + roomName + "' not pupulated yet." + LINE_DEB);
				call.openRoom(roomName);
			} else { logger.info(KURENTO_DEB + "Room '" + roomName + "' already populated." + call + LINE_DEB); }
			
			//Links the caller (getFrom()) to the Media Server.
			String sdpAnswer = call.link(fromName, voip1_rtp1);
			SipServletResponse response = req.createResponse(200);
			logger.info(KURENTO_DEB + "SDP ANSWER:\n " + sdpAnswer + LINE_DEB);
			response.setContent(sdpAnswer, "application/sdp");
			logger.info(KURENTO_DEB + "200 OK generated" + LINE_DEB);
			response.send();
			logger.info(KURENTO_DEB + "200 OK sent." + LINE_DEB);
			logger.info(KURENTO_DEB + "Current N. of users on call: " + call.getUsersNumber() + LINE_DEB);
			callContainer.put(roomName, call);
// GROUP CALL - END
			
		 } else if (KurentoSupport.contains(req.getTo().toString(), "kurento")) {
				logger.warn("\n\n☎ ☎ ☎ ☎ ☎  User has called KURENTO ☎ ☎ ☎ ☎ ☎\n");
				KurentoClient kurento = KurentoSupport.connect();
				logger.info(KURENTO_DEB + "Connected to Kurento with direct [ PLAYER -> RTP connection ]." + LINE_DEB );
				MediaPipeline pipeline = kurento.createMediaPipeline();			
				RtpEndpoint rtpEP = new RtpEndpoint.Builder(pipeline).build();			
				logger.info(KURENTO_DEB + "RtpEndpoint created." + LINE_DEB );
				PlayerEndpoint player = new PlayerEndpoint.Builder(pipeline, file_uri).build();
				logger.info(KURENTO_DEB + "PlayerEndpoint created." + LINE_DEB );
				player.connect(rtpEP);
				String inSdpAnswer = rtpEP.processOffer(voip1_rtp1);
				logger.info(KURENTO_DEB + " Offer for RtpEndpoint processed by itself. " + LINE_DEB );
				SipServletResponse response = req.createResponse(200);
				response.setContent(inSdpAnswer, "application/sdp");
				logger.info(KURENTO_DEB + "200 OK generated" + LINE_DEB);
				player.play();
				logger.info(KURENTO_DEB + "Player played." + LINE_DEB);
				response.send();
				logger.info(KURENTO_DEB + "200 OK sent." +  KURENTO_DEB);
			
		}	 else {  //★ ★ ★ ★ ★ ★ ★ ★ ★ ★ Normal Call ★ ★ ★ ★ ★ ★ ★ ★ ★ ★
			logger.warn("\n\n☎ ☎ ☎ ☎ ☎  User has made NORMAL CALL ☎ ☎ ☎ ☎ ☎\n");
			KurentoClient kurento = KurentoSupport.connect();
			
			MediaPipeline pipeline = kurento.createMediaPipeline();
			Composite composite = new Composite.Builder(pipeline).build();
			HubPort hub1 = new HubPort.Builder(composite).build();
		    HubPort hub2 = new HubPort.Builder(composite).build();
		   RtpEndpoint rtp1 = new RtpEndpoint.Builder(pipeline).build();
			RtpEndpoint rtp2 = new RtpEndpoint.Builder(pipeline).build();
			logger.info(DEBUG + "RtpEndpoint connected bidirectionally." + LINE_DEB );
			String rtp1_to_voip1 = rtp1.processOffer(voip1_rtp1);
			logger.info(DEBUG + "SDP ANSWER FROM KURENTO [ VoIP(1) -> Kurento ]:\n" + rtp1_to_voip1 + LINE_DEB);
			
// 	 Generating 200 OK for the first VoIP.
			SipServletResponse generatedResp = req.createResponse(200);
			generatedResp.setContent(rtp1_to_voip1, "application/sdp");
			logger.info(DEBUG + "200 OK generated:\n" + generatedResp.toString() + LINE_DEB + "It will be send later." + LINE_DEB);

			
// INVITE
			logger.info(DEBUG + "Instantiating INVITE from Mobicents to VoIP(2). " + LINE_DEB);
			//"to" e "from" sono due Array. Chiedo alla sipFactory di creare gli oggetti URI
			URI from = sipFactory.createAddress(req.getFrom().toString()).getURI();    
			String toURI = req.getTo().getURI().toString();
			URI to = registeredUsers.get(toURI);
			//this line print "from", "toURI" and "URI" parameters.
			logger.info(DEBUG + "From: " + from + "\ntoURI: " + toURI + "\nto: " + to + LINE_DEB);
			/* ************************* creation of INVITE message ****************************************** */
	        SipServletRequest invite = sipFactory.createRequest(req.getApplicationSession(), "INVITE" , from , to);
	        /* ************************************************************************************************* */
			logger.info(DEBUG+ "Sending INVITE From: " + req.getFrom().toString() + " To: " + req.getRequestURI() + LINE_DEB);
			invite.send();
//	INVITE-END	
			
			logger.warn("\n\n★ ★ ★ ★ ★ ★ ★ ★   INVITE sent   ★ ★ ★ ★ ★ ★ ★ ★");
			invite.getSession().setAttribute("generatedResp", generatedResp);
			invite.getSession().setAttribute("rtp1Object", rtp1);
			invite.getSession().setAttribute("rtp2Object", rtp2);
			invite.getSession().setAttribute("hub1Object", hub1);
			invite.getSession().setAttribute("hub2Object", hub2);
			invite.getSession().setAttribute("rtp1_to_voip1Object", rtp1_to_voip1);
			invite.getSession().setAttribute("kurento", kurento);
			session.setAttribute("LinkedSession", invite.getSession());      //Session = settata alla sessione di INVITE creato dettata da RESP
			invite.getSession().setAttribute("LinkedSession", session);
		}
	}

	
////////////////////////////////////////// RESPONSE/////////////////////////////////////////
	@Override
	protected void doSuccessResponse(SipServletResponse resp) 
																
			throws ServletException, IOException {
		SipSession session = resp.getSession();
		SipSession linkedSession = (SipSession) session.getAttribute("LinkedSession");
		SipServletResponse generatedResp = (SipServletResponse) session.getAttribute("generatedResp");
		
		SipServletRequest ack = resp.createAck();
		
		RtpEndpoint rtp1 = (RtpEndpoint) session.getAttribute("rtp1Object");
		RtpEndpoint rtp2 = (RtpEndpoint) session.getAttribute("rtp2Object");
		HubPort hub1 = (HubPort) session.getAttribute("hub1Object");
		HubPort hub2 = (HubPort) session.getAttribute("hub2Object");
		KurentoClient kurento = (KurentoClient) session.getAttribute("kurento");
		if (resp.getStatus() == SipServletResponse.SC_OK) {
			logger.warn("\n\n★ ★ ★ ★ ★ ★ ★ ★   2 0 0   O K   ★ ★ ★ ★ ★ ★ ★ ★ ★ ★\n" +
					"The VoIP (2) has answered with 200 OK and a SDP that will be sent to Kurento: "+  resp.toString() + LINE_DEB);
			
			//getting SDP from Response. Obtaining the Contend of SDP and putting in VoIP(2)->RTP2:
			String voip2_rtp2 = new String(resp.getRawContent());
			//RTP2->VoIP(2) will be the answer of Kurento:
			String rtp2_voip2  = rtp2.processOffer(voip2_rtp2);
			ack.setContent(voip2_rtp2, "application/sdp");
			
			logger.info(DEBUG + "So the reprocessed SDP is:\n" + rtp2_voip2 +
					"\n\nTrying to send 200 OK." + LINE_DEB);
			generatedResp.send();
			logger.warn("\n\n★ ★ ★ ★ ★ ★ ★ ★ 200 OK sent. ★ ★ ★ ★ ★ ★ ★ ★\n");			
			generatedResp.getSession().setAttribute("generatedACK", ack);
			generatedResp.getSession().setAttribute("rtp1Object", rtp1);
			generatedResp.getSession().setAttribute("rtp2Object", rtp2);
			generatedResp.getSession().setAttribute("hub1Object", hub1);
			generatedResp.getSession().setAttribute("hub2Object", hub2);
			generatedResp.getSession().setAttribute("kurento", kurento);
			session.setAttribute("LinkedSession", ack.getSession());      //Session = settata alla sessione di INVITE creato dettata da RESP
			ack.getSession().setAttribute("LinkedSession", session);
		}
	}
	
	protected void doAck(SipServletRequest req) throws ServletException, IOException {
		
		SipSession session = req.getSession();
		SipSession linkedSession = (SipSession) session.getAttribute("LinkedSession");
		SipServletRequest ack = (SipServletRequest) session.getAttribute("generatedACK");
		String roomName = KurentoSupport.stringfy(req.getFrom().toString());
		if (KurentoSupport.contains(req.getTo().toString(), "groupcall")) {
			logger.info(DEBUG + "Received ACK for GROUPCALL . Not need to answer with Kurento." + LINE_DEB);
		} else if (roomName == "kurento") {
			logger.info(DEBUG + "Received ACK for KURENTO or KURENTOHUB. Not need to answer with Kurento." + LINE_DEB);
		} else {
			RtpEndpoint rtp1 = (RtpEndpoint) session.getAttribute("rtp1Object");
			RtpEndpoint rtp2 = (RtpEndpoint) session.getAttribute("rtp2Object");
			HubPort hub1 = (HubPort) session.getAttribute("hub1Object");
			HubPort hub2 = (HubPort) session.getAttribute("hub2Object");
			KurentoClient kurento = (KurentoClient) session.getAttribute("kurento");
			rtp1.connect(hub1);
			hub1.connect(rtp1);
			rtp2.connect(hub2);
			hub2.connect(rtp2);
			logger.info(DEBUG + "Received ACK.  Sending the ack to VoIP(2)." + req.toString() + LINE_DEB);
			ack.send();
			logger.warn("\n\n★ ★ ★ ★ ★ ★ ★ ★ ACK sent. ★ ★ ★ ★ ★ ★ ★ ★\n");
			ack.getSession().setAttribute("kurento", kurento);
			session.setAttribute("LinkedSession", ack.getSession());      //Session = settata alla sessione di INVITE creato dettata da RESP
			ack.getSession().setAttribute("LinkedSession", session);
		}
	}

	@Override
	protected void doOptions(SipServletRequest req) throws ServletException,
			IOException {
		logger.info(DEBUG + "Got Option:  " + req.toString() + LINE_DEB);
		req.createResponse(SipServletResponse.SC_OK).send();
	}
	
	@Override
	protected void doErrorResponse(SipServletResponse resp) throws ServletException,
			IOException {
		// If someone rejects it remove the call from the table
		CallStatusContainer calls = (CallStatusContainer) getServletContext().getAttribute("activeCalls");
		calls.removeCall(resp.getFrom().getURI().toString(), resp.getTo().getURI().toString());
		calls.removeCall(resp.getTo().getURI().toString(), resp.getFrom().getURI().toString());

	}
	
	@Override
	protected void doBye(SipServletRequest req) throws ServletException,
			IOException {
		logger.info(DEBUG + "Got bye:\n" + req.toString() + LINE_DEB);
		//SipSession session = req.getSession();
		String roomName = KurentoSupport.stringfy(req.getTo().toString());
		String fromName = KurentoSupport.stringfy(req.getFrom().toString());
		if (KurentoSupport.contains(req.getTo().toString(), "kurento")) {
// KURENTO
			logger.info(KURENTO_DEB + "Received BYE. It will not be re-send becase Kurento doesn't need it.");
		} 
		else if (KurentoSupport.contains(req.getTo().toString(), "groupcall")) {
			roomName = GroupCall.stringfy(req.getTo().toString());
// GROUP CALL
			logger.info(KURENTO_DEB + "It's a group call BYE to room: '" + roomName +"'."+  LINE_DEB);
			
			GroupCall call = callContainer.get(roomName);
			call.release(fromName);
			if  (call.getUsersNumber()== 0) {
				call.closeRoom();
				callContainer.remove(roomName);
				logger.info(KURENTO_DEB + "Room destroyed." + LINE_DEB);
				if (GroupCall.getGlobalUsersNumber() == 0) {
					GroupCall.disconnect();
					call = null;
				}
			}
// GROUP CALL END
		}
		
		else {			
			SipSession session = req.getSession();
			SipSession linkedSession = (SipSession) session.getAttribute("LinkedSession");
			if (linkedSession != null) {
				SipServletRequest bye = linkedSession.createRequest("BYE");
				logger.info(DEBUG + "Sending bye to " + session.getRemoteParty());
				bye.send();
				logger.info("\n\n★★★★★★★★ BYE sent. ★★★★★★★★\n" + LINE_DEB);
			}
		
		/*
		 * CallStatusContainer calls = (CallStatusContainer) getServletContext().getAttribute("activeCalls");
		calls.removeCall(req.getFrom().getURI().toString(), req.getTo().getURI().toString());
		calls.removeCall(req.getTo().getURI().toString(), req.getFrom().getURI().toString());
		*/
		
		
		/*
		SipServletRequest bye = session.createRequest("BYE");
		logger.info(DEBUG + "Sending bye to " + req.getTo().toString() + LINE_DEB);
		bye.send();
		*/
			logger.info(DEBUG + "Sending ok to who sent the ACK....");
		SipServletResponse ok = req.createResponse(SipServletResponse.SC_OK);
		ok.send();
		logger.info("\n\n★★★★★★★★ 200 OK sent. ★★★★★★★★\n");
		} 
	}
    
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doResponse(SipServletResponse response)
			throws ServletException, IOException {

		logger.info("SimpleProxyServlet: Got response:\n" + response);
		super.doResponse(response);
	}


	/**
	 * {@inheritDoc}
	 */
	public void noAckReceived(SipErrorEvent ee) {
		logger.info("SimpleProxyServlet: Error: noAckReceived.");
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void noPrackReceived(SipErrorEvent ee) {
		logger.info("SimpleProxyServlet: Error: noPrackReceived.");
	}
	
	protected void doRegister(SipServletRequest req) throws ServletException, IOException {
		logger.info(DEBUG + "Received register request: " + req.getTo() + ". " + LINE_DEB);
		int response = SipServletResponse.SC_OK;
		SipServletResponse resp = req.createResponse(response);
		if(registeredUsers == null) registeredUsers = new Hashtable<String, URI>();
		getServletContext().setAttribute("registeredUsersMap", registeredUsers);
		Address address = req.getAddressHeader(CONTACT_HEADER);
		String fromURI = req.getFrom().getURI().toString();
		int expires = address.getExpires();
		if(expires < 0) {
			expires = req.getExpires();
		}
		if(expires == 0) {
			registeredUsers.remove(fromURI);
			logger.info("User " + fromURI + " unregistered");
		} else {
			resp.setAddressHeader(CONTACT_HEADER, address);
			/* Populating Map of registered users.***************************************************** */
			registeredUsers.put(fromURI, req.getAddressHeader(CONTACT_HEADER).getURI());
			/* ***************************************************************************************** */
			logger.info(DEBUG + "User: " + fromURI +" registered with an Expire time of " + expires + ". " + LINE_DEB);
		}				
		resp.send();
	}
}