package support;

import java.util.HashMap;
import java.util.Map;

import org.kurento.client.Composite;
import org.kurento.client.HubPort;
import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;
import org.kurento.client.PlayerEndpoint;
import org.kurento.client.RtpEndpoint;
import org.apache.log4j.Logger;

public class GroupCall {
	
	//Player for Voice Mail to the First  users on call.
	private PlayerEndpoint voiceMail; //= new PlayerEndpoint.Builder(pipeline, "http://video.webmfiles.org/elephants-dream.webm").build();
	private HubPort hubForVoiceMail; // = new HubPort.Builder(composite).build();
	
	//handle the Status of he VoiceMail status.
	private boolean voiceMailStatus;
	private Composite composite;
	private MediaPipeline pipeline;
	private  int roomUsersNumber;
	private String roomName;
	// HUB e RTP contengono <from , KurentoEndPoint >
		private  Map <String, HubPort> hubContainer;
		private  Map<String, RtpEndpoint> rtpContainer;
	
	private static KurentoClient kurento;
	private static int globalUsersNumber;
	private static Logger logger = Logger.getLogger(GroupCall.class);
	private static final String KURENTO_DEB = "\n\n✩ ✫ ✬ ✭ ✮ ✰ ☆ D   E   B   U   G   ✩ ✫ ✬ ✭ ✮ ✰ ☆\n\n";
	private static final String LINE_DEB = "\n\n=============================\n";

	public static void connect() {
		globalUsersNumber = 0;
		kurento = KurentoSupport.connect();
		
	}

	public static void disconnect() {
		KurentoSupport.disconnect();
		globalUsersNumber = 0;
		logger.info(KURENTO_DEB + "Disconnected from Kurento." + LINE_DEB);
	}

	public void openRoom(String roomName) {
		this.roomUsersNumber = 0;
		this.pipeline = kurento.createMediaPipeline();
		this.composite = new Composite.Builder(pipeline).build();
		this.hubContainer = new HashMap<String, HubPort>();
		this.rtpContainer = new HashMap<String, RtpEndpoint>();
		this.voiceMailStatus = true;
		this.roomName = roomName;
		logger.info(KURENTO_DEB + " Room '" + roomName + "' populated." + LINE_DEB);
	}

	public String link(String fromName, String sdp) {
		logger.info(KURENTO_DEB + "Adding user '" + fromName +"' to room '" + roomName +"'." + LINE_DEB);
		RtpEndpoint rtp = new RtpEndpoint.Builder(pipeline).build();
		HubPort hub = new HubPort.Builder(composite).build();
		String sdpOut = rtp.processOffer(sdp);

		if (this.roomUsersNumber == 0) {
			this.voiceMail = new PlayerEndpoint.Builder(pipeline, "http://video.webmfiles.org/elephants-dream.webm").build();
			this.hubForVoiceMail = new HubPort.Builder(composite).build();
			this.voiceMail.connect(hubForVoiceMail);
			this.voiceMail.play();
			this.voiceMailStatus = true;

		} else if (this.voiceMailStatus == true) {
			logger.info(KURENTO_DEB + "Voice mail of room ''"  + this.roomName + "': " + voiceMailStatus + LINE_DEB);
			this.voiceMail.stop();
			this.voiceMail.release();
			this.voiceMail = null;
			this.hubForVoiceMail.release();
			this.hubForVoiceMail.release();
			this.voiceMailStatus = false;
			}

		rtp.connect(hub);
		hub.connect(rtp);
		this.hubContainer.put(fromName, hub);
		this.rtpContainer.put(fromName, rtp);
		this.roomUsersNumber++;
		globalUsersNumber++;
		logger.info(KURENTO_DEB + "Added User " + fromName + " to Room: '" + this.roomName +  "'\nN. of users: "
				+ this.roomUsersNumber+ "\nGlobal N. of users: " + globalUsersNumber + LINE_DEB);
		return sdpOut;
	}

	public void release(String fromName) {
		HubPort hub = hubContainer.get(fromName);
		RtpEndpoint rtp = rtpContainer.get(fromName);
		hub.disconnect(rtp);
		rtp.disconnect(hub);
		hub.release();
		rtp.release();
		// Toglie dai container RTP e HUB gli oggetti che riferiscono
		// all'utente, preso per TAG.
		this.rtpContainer.remove(fromName);
		this.hubContainer.remove(fromName);
		if (this.voiceMailStatus == true){
			logger.warn(KURENTO_DEB + "Voice mail of room ''"  + this.roomName + "': " + voiceMailStatus + LINE_DEB);
			this.voiceMail.stop();
			this.voiceMail.release();
			this.voiceMail = null;
			this.hubForVoiceMail.release();
			this.hubForVoiceMail.release();
			this.voiceMailStatus = false;
			logger.info(KURENTO_DEB + "Voice mail of room ''"  + this.roomName + "' released. " + LINE_DEB);
		}
		this.roomUsersNumber--;
		globalUsersNumber--;
		logger.info(KURENTO_DEB + "Removed User " + fromName + " to Room.'" + "\nUser on call in room: " 
				+ roomUsersNumber 
				+ "\nGlobal N. of users in the server: " + globalUsersNumber +  LINE_DEB);
	}

	public void closeRoom() {
		this.composite.release();
		this.pipeline.release();
		logger.info(KURENTO_DEB + "Room destroyed." + LINE_DEB);
	}

	//public static boolean getConnection() { return isConnected; }

	public static int getGlobalUsersNumber() { return globalUsersNumber; }

	public static void setGlobalUsersNumber(int globalNumber) { globalUsersNumber = globalNumber; }

	public static String stringfy(String address) {
		int left = address.indexOf("-");
		int right = address.indexOf("@");

		// pull out the text inside the parens
		return address.substring(left + 1, right);
	}

	public int getUsersNumber() {
		return this.roomUsersNumber;
	}

	public void setUsersNumber(int usersCounter) {
		this.roomUsersNumber = usersCounter;
	}

	public String getRoomName() {
		return this.roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
}