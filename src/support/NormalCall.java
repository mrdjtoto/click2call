package support;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.kurento.client.Composite;
import org.kurento.client.HubPort;
import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;
import org.kurento.client.RtpEndpoint;

public class NormalCall {

	private MediaPipeline pipeline;
	private  int roomUsersNumber;
	private String roomName;
	// HUB e RTP contengono <from , KurentoEndPoint >
		private  Map <String, HubPort> hubContainer;
		private  Map<String, RtpEndpoint> rtpContainer;
	
	private static KurentoClient kurento;
	private static int globalUsersNumber;
	private static Logger logger = Logger.getLogger(GroupCall.class);
	private static final String KURENTO_DEB = "\n\n✩ ✫ ✬ ✭ ✮ ✰ ☆ D   E   B   U   G   ✩ ✫ ✬ ✭ ✮ ✰ ☆\n\n";
	private static final String LINE_DEB = "\n\n=============================\n";
	
	public static void connect() {
		globalUsersNumber = 0;
		kurento = KurentoSupport.connect();
	}
	
	public static void disconnect() {
		KurentoSupport.disconnect();
		globalUsersNumber = 0;
		logger.info(KURENTO_DEB + "Disconnected from Kurento." + LINE_DEB);
	}
	
	public void openRoom(String roomName) {
		this.roomUsersNumber = 0;
		this.pipeline = kurento.createMediaPipeline();
		this.hubContainer = new HashMap<String, HubPort>();
		this.rtpContainer = new HashMap<String, RtpEndpoint>();
		this.roomName = roomName;
		logger.info(KURENTO_DEB + " Room '" + roomName + "' populated." + LINE_DEB);
	}
	
	public void closeRoom() {
		this.pipeline.release();
		logger.info(KURENTO_DEB + "Room destroyed." + LINE_DEB);
	}

	//public static boolean getConnection() { return isConnected; }

	public static int getGlobalUsersNumber() { return globalUsersNumber; }

	public static void setGlobalUsersNumber(int globalNumber) { globalUsersNumber = globalNumber; }

	public static String stringfy(String address) {
		int left = address.indexOf("-");
		int right = address.indexOf("@");

		// pull out the text inside the parens
		return address.substring(left + 1, right);
	}

	public int getUsersNumber() {
		return this.roomUsersNumber;
	}

	public void setUsersNumber(int usersCounter) {
		this.roomUsersNumber = usersCounter;
	}

	public String getRoomName() {
		return this.roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
