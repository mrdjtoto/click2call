package support;

import org.apache.log4j.Logger;
import org.kurento.client.KurentoClient;



public class KurentoSupport {
	
	public static boolean connectionStatus = false;
	private static KurentoClient kurento;
	private static Logger logger = Logger.getLogger(KurentoSupport.class);
	private static final String KURENTO_DEB = "\n\n✩ ✫ ✬ ✭ ✮ ✰ ☆ D   E   B   U   G   ✩ ✫ ✬ ✭ ✮ ✰ ☆\n\n";
	private static final String LINE_DEB = "\n\n=============================\n";
	
	public static KurentoClient connect() {
		
		//KurentoClient kurento = null;
		
		if (!connectionStatus) {
			kurento = KurentoClient.create(System.getProperty("kms.ws.uri", "ws://192.168.1.10:8888/kurento"));
			connectionStatus = true;
			logger.info(KURENTO_DEB + "Connected to Kurento." + LINE_DEB);
		} else { 
			logger.info(KURENTO_DEB + "Kurento Already connected." + LINE_DEB); 
			}
		return kurento;
	}
	
public static void disconnect() {
		
		//KurentoClient kurento = null;
		
		if (connectionStatus) {
			kurento.destroy();
			connectionStatus = false;
			logger.info(KURENTO_DEB + "Disconnected to Kurento." + LINE_DEB);
		} else {
			logger.info(KURENTO_DEB + "Kurento Already disconnected." + LINE_DEB); 
		}

	}
	
	public static String stringfy(String address){
		int left = address.indexOf(":");
		int right = address.indexOf("@");

		// pull out the text inside the parens
		String sub = address.substring(left+1, right); 
		return sub;
	}
	
	public static boolean contains( String haystack, String needle ) {
		  haystack = haystack == null ? "" : haystack;
		  needle = needle == null ? "" : needle;
		  return haystack.toLowerCase().contains( needle.toLowerCase() );
		}
}

